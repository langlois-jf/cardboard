# SPDX-License-Identifier: GPL-3.0-only
pixman = dependency('pixman-1')
wayland_server = dependency('wayland-server')
xkbcommon = dependency('xkbcommon')
xcb = dependency('xcb', required: get_option('xwayland'))

wlroots = dependency('wlroots', version: '>= 0.15')

if get_option('xwayland').enabled() and wlroots.get_variable('have_xwayland') != 'true'
    error('Cannot enable Xwayland support in cardboard: wlroots has been built without Xwayland support')
endif
have_xwayland = xcb.found()

conf_data = configuration_data()
conf_data.set10('HAVE_XWAYLAND', have_xwayland)

configure_file(output: 'BuildConfig.h', configuration: conf_data)

cardboard_deps = [
  expected,
  pixman,
  server_protos,
  wayland_server,
  wlroots,
  xkbcommon,
]

cardboard_sources = files(
  'Cursor.cpp',
  'IPC.cpp',
  'Keyboard.cpp',
  'Layers.cpp',
  'Output.cpp',
  'OutputManager.cpp',
  'Seat.cpp',
  'Server.cpp',
  'Spawn.cpp',
  'View.cpp',
  'Workspace.cpp',
  'XDGView.cpp',
  'ViewOperations.cpp',
  'ViewAnimation.cpp',
  'SurfaceManager.cpp',
  'main.cpp',
  'commands/dispatch_command.cpp'
)

if have_xwayland
    cardboard_deps += xcb
    cardboard_sources += 'Xwayland.cpp'
endif

subdir('wlr_cpp_fixes')

executable(
  'cardboard',
  cardboard_sources,
  include_directories: [wlr_cpp_fixes_inc, libcardboard_inc],
  dependencies: cardboard_deps,
  link_with: libcardboard,
  install: true
)

{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/master";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, flake-utils }: flake-utils.lib.simpleFlake {
    inherit self nixpkgs;
    name = "cardboard";
    overlay = final: prev: {
      cardboard = {
        defaultPackage = prev.stdenv.mkDerivation rec {
          name = "cardboard-${version}";
          version = "0.0.1";
          src = ./.;
          buildInputs = with final; [
            cereal
            libGL
            libdrm
            libffi
            libinput
            libxkbcommon
            mesa.dev
            pixman
            tl-expected
            wayland
            wayland-protocols
            wlroots
            xorg.libxcb.dev
          ];
          nativeBuildInputs = with final; [ meson ninja pkg-config cmake ];
        };
      };
    };
    shell = { pkgs }: with pkgs; mkShell {
      inputsFrom = [ cardboard.defaultPackage ];
      packages = [ ccls gdb ];
      hardeningDisable = [ "all" ];
    };
  };
}
